import React, { useEffect, useState } from 'react';
import axios from 'axios'
import './App.css';
import Layout from './layout/layout';
import VacancyList from './pages/vacancyList/VacancyList';
import data from './vacancies.json';
import { Container,  Pagination, TextField, Stack, Link } from '@mui/material';

const BASE_URL = 'http://localhost:3001/vacancy/';
function App() {
  const [vacancies, setVacancies] = useState([]);
  const [query, setQuery] = useState('react');
  const [page, setPage] = useState(1);
  const [pageQty, setPageQty]= useState(0);


  const count = '20'
  useEffect(()=>{
    axios.get(BASE_URL + '?p=' + (page-1)+ '&count=' + count).then(
      ({data}) => {
        console.log(data)
        setVacancies(data)
        setPageQty(data.pageCount)
      }
    )
  }, [page])


  

  return (
    <Container>
      <Layout Ai="AI" Name='Крутой проект' Info='Справка'>
      </Layout>
      {vacancies.data && <VacancyList vacancies={vacancies.data} />}
      <Stack spacing={2}>
        {!!pageQty && (
          <Pagination
          count={pageQty}
          page={page}
          showFirstButton
          showLastButton
          onChange={(_, num) => setPage(num)}
          />
        )}
      </Stack>
    </Container>
  );
}

export default App;
