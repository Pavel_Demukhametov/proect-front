import React from 'react';
import styles from './Header.module.css';

function Header(props) {
  return (
    <header>
      <div className={styles.Home}>
        <div className={styles.Logo}>{props.Ai}</div>
        <div className={styles.Name}>{props.Name}</div>
      </div>
      <div className={styles.Info}>{props.Info}</div>
    </header>
  );
}

export default Header;