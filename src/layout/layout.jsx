import React from 'react';
import Header from '../components/header/Header';
import { Outlet, Link } from "react-router-dom";

const Layout = (props) => {
    return (
      <>
        <Header Ai={props.Ai} Name={props.Name} Info={props.Info}/>
        <Outlet />
      </>
    )
  }
  
  export default Layout
  